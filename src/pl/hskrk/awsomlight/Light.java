package pl.hskrk.awsomlight;
/***********************************************************
 * Klasa opisuj�ca pojedynczy prze��cznik                  *
 * Basic class describing single light toggle			   *
 **********************************************************/
public class Light {
	private String id;//zawiera nazw� prze��cznika(�wiat�e na kt�re oddzia�uje)
					  //Contains name of Light bulb that is switched by described toggle
	private boolean on;//Stan logiczny �wiat�a(w��czone/wy��czone)
					   //Tells you if light is on or off
	//Konstruktor/Constructor
	public Light(String id){
		this.id = id;
		on = false;
	}
	public Light(String id,boolean on){
		this.id = id;
		this.on = on;
	}
	//Prz곹cznie �wiat�a
	//Toggling the light
	public void toggle(){
		if(on){
			//Switch off
			on = false;
		}else{
			//Switch on
			on = true;
		}
		//Tutaj si� walnie ob�ug� wyj�tk�w
	}
	public String hasBeenLit(){
		if(on) return("on");
		else return("off");
	}
	public boolean getState(){
		return on;
	}
	public void setState(boolean has_been_lit){
		on = has_been_lit;
	}
	public String getName(){
		return id;
	}
	
	public String toString(){
		return(id +'('+this.hasBeenLit()+')');
		//return id;
	}
}
