package pl.hskrk.awsomlight;
/*******************************************
 * Główna okno aplikacji                   *
 * Main view of app						   *
 *******************************************/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class MainActivity extends Activity {
	/*******************************************************
	* to do:                                               *
    * Wczytywanie listy przełączników z configa.xml        *  
	* Kostruowanie Layoutu przy włączeniu aplikacji        *
	* Wczytywanie ssid z configa                           *
	* Loading list of toggles from connfig.xml             *
	* Building lauout based on list of toggles             *
	* getting ssid from config                             *
	*******************************************************/
    
    
	public final String UrlToggle = "http://light.local/api/v1/toggleByDescription/";
	public final String UrlGet = "http://light.local/api/v1/getByDescription/"; 
	private ArrayAdapter<String> LightsAdapter;
	private static ArrayList<Light> Lights;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		LightsAdapter = new ArrayAdapter<String>(this,R.layout.list_element);
		Lights = new ArrayList<Light>();
		Lights.add(new Light("hardroom"));
		Lights.add(new Light("softroom"));
		Lights.add(new Light("corridor"));
		Lights.add(new Light("kitchen"));
		
		ListView LightsList = (ListView) findViewById(R.id.listView1);
		LightsList.setAdapter(LightsAdapter);
		LightsList.setOnItemClickListener(ToggleClickListener);
		
		
		
	}
	@Override
	protected void onStart(){
		super.onStart();
		LightsAdapter.clear();
		if(Lights.size() > 0){
			for(Light light : Lights){
				if(isConnected()){
					String stringUrl = UrlGet+light.getName();
		            new WebConnectionTask().execute(stringUrl);
				}
				String s = light.toString();
				LightsAdapter.add(s);
			}
		}
	}
	private OnItemClickListener ToggleClickListener = new OnItemClickListener() {
	    public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

	        String info = ((TextView) v).getText().toString();
	        LightsAdapter.clear();
	        if(Lights.size() > 0){
				for(Light light : Lights){
					if(info.contains(light.getName())){ 
				        String stringUrl = UrlToggle+light.getName();
				        if (isConnected()) {
				            new WebConnectionTask().execute(stringUrl);
				        }
				        light.toggle();
					}else{
						if (isConnected()) {
							String stringUrl = UrlGet+light.getName();
				            new WebConnectionTask().execute(stringUrl);
				        }
					}
					String s = light.toString();
					LightsAdapter.add(s);
				}
			}

       
	        
	    }
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	private boolean isConnected(){
		ConnectivityManager	connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		 if (networkInfo != null && networkInfo.isConnected()){
			 return true;
		 }else{
			 return false;
		 }
	}
	 static class WebConnectionTask extends AsyncTask<String, Void, Boolean> {

		String url;
		@Override
		protected Boolean doInBackground(String... urls) {
            url =urls[0];
			return GET(urls[0]);
		}
        @Override
        protected void onPostExecute(Boolean result) {
            for(Light light : Lights){
            	if(url.contains(light.getName())){
            		light.setState(result);
            	}
            }
       }
        public static Boolean GET(String url){
            InputStream inputStream = null;
            String result = "";
            try {
     
                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();
     
                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
     
                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
     
                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else{
                    result = "Did not work!";
                }
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
     
            if(result.equals("1")){
            	return true;
            }else{
            	return false;
            }
        }
     
        // convert inputstream to String
        private static String convertInputStreamToString(InputStream inputStream) throws IOException{
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;
     
            inputStream.close();
            return result;
     
        }
	};
}
